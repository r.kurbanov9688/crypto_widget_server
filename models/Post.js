import mongoose from 'mongoose';

const PostShema = mongoose.Schema({
    date: {
        type: Date
    },
    currencyFrom: {
        type: String,
        required: true
    },
    amount1: {
        type: Number,
        required: true
    },
    currencyTo: {
        type: String,
        required: true
    },
    amount2:{
        type: Number,
        required: true
    },
    type:{
        type: String,
        required: true
    },
})

export default mongoose.model('post', PostShema)

