# Crypto widget server

---

Server for widget which is used to display several currency exchanges rates and it allows the user to exchange USD for Crypto.

## Getting started

---

Install Node js from https://nodejs.org/en/download/

Clone the repository with command

`git clone https://gitlab.com/r.kurbanov9688/crypto_widget_server.git`

Go to the project folder and install the dependencies with command

`npm install`

Run project with command

`npm start`

Server will run on http://localhost:3004

## Technologies

---

- Express
- Axios
- MongoDB

## .env variables

DB_CONNECTION used for connection to database



## Available Scripts

In the project directory, it's possible to run:



### `npm start`

Runs the server on http://localhost:3004

### `npm run dev`

Runs the server on http://localhost:3004 and automatically restarting the application when file changes in the directory are detected


