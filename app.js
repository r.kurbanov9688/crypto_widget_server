import express from 'express';
import axios from 'axios';
import mongoose from 'mongoose';
import 'dotenv/config';
import cors from 'cors';
import bodyParser from 'body-parser';
import Post from './models/Post.js';


const app = express();
app.use(cors());
app.use(bodyParser.json());
const port = 3004;

const getRate = (currency) => axios.get(`https://rest.coinapi.io/v1/exchangerate/${currency}/USD`, {
    headers:  {
        'X-CoinAPI-Key': '206573DC-88C1-41E4-9E31-EF9AC45A888B',
    }
});

mongoose.connect(process.env.DB_CONNECTION, () => {
    console.log('connected to DB');
});


app.get('/rate', async (req, res) => {
    const ethereum = await getRate('ETH')
    const bitcoin = await getRate('BTC')
    if (ethereum && bitcoin) {
        const rate = [
            {
                'Bitcoin': bitcoin.data.rate,
                'Ethereum': ethereum.data.rate
            }
        ];
        res.send(rate);
    }
});

app.get('/history', async (req, res) => {
    const posts = await Post.find();
    res.json(posts);
});

app.post('/history', async (req, res) => {
    const post = new Post({
        date: req.body.date,
        currencyFrom: req.body.currencyFrom,
        amount1: req.body.amount1,
        currencyTo: req.body.currencyTo,
        amount2: req.body.amount2,
        type: req.body.type
    });
    const savedPost = await post.save();
    res.json(savedPost);
});

app.listen(port, () => {
    console.log(`Crypto widget server listening on port ${port}`);
});